/*
 *  Attribut.h
 *
 *  Module defining the type of attributes in
 *  symbol table.
 *
 */

#ifndef ATTRIBUT_H
#define ATTRIBUT_H

#include <stdio.h>

typedef struct
{
   int values_count;
   int *values;
} symb_value_type;
/* Dummy definition of symbol_value_type.
   Could be instead a structure with as many fields
   as needed for the compiler such as:
   - name in source code
   - name (or position in the stack) in the target code
   - type (if ever)
   - other info....
*/

symb_value_type* svt_create(int first_value);
void svt_free(symb_value_type* sv);
void svt_add_value(symb_value_type* sv, int value);
int svt_get_last_value(symb_value_type* sv);
void svt_remove_last_value(symb_value_type* sv);
void svt_print(symb_value_type* sv);

#endif
