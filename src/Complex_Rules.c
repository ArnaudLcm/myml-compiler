#include "Complex_Rules.h"

#include "Table_des_symboles.h"
#include <string.h>
#include <stdlib.h>

int offset = 0;

int offset_save = 0;
char **fun_params = NULL;
int fun_params_size = 0;

int label_count = 0;

extern FILE *file_out;
extern FILE *file_f_out;

void stdout_to_file()
{
    stdout = file_out;
}

void stdout_to_f_file()
{
    stdout = file_f_out;
}

void cr_def_id(char *sid)
{
    if (is_symbol_stored(sid))
    {
        printf("STORE(fp + %d) //%s\n", svt_get_last_value(get_symbol_value(sid)), sid);
        offset--;
    }
    else
    {
        symb_value_type *symb_value = svt_create(offset - 1);
        set_symbol_value(sid, symb_value);
    }
    // symb_table_print();
}

void cr_local_def_id(char *sid)
{
    if (is_symbol_stored(sid))
    {
        svt_add_value(get_symbol_value(sid), offset - 1);
    }
    else
    {
        symb_value_type *symb_value = svt_create(offset - 1);
        set_symbol_value(sid, symb_value);
    }
}

void cr_let_exp(char *sid)
{
    printf("DRCP //%s\n", sid);
    offset--;
    remove_last_value_or_symbol_if_needed(sid);
}

void cr_then()
{
    if (!is_symbol_stored(string_to_sid(":label_stack")))
        set_symbol_value(string_to_sid(":label_stack"), svt_create(label_count));

    printf("IFN(F%d)\n", label_count);
    offset--;
    svt_add_value(get_symbol_value(string_to_sid(":label_stack")), label_count);
    label_count++;
}

void cr_else()
{
    if (!is_symbol_stored(string_to_sid(":label_stack")))
        set_symbol_value(string_to_sid(":label_stack"), svt_create(label_count));

    // printf("%d----\n", is_symbol_stored(string_to_sid(":label_stack")));
    // symb_table_print();

    symb_value_type *symb_value = get_symbol_value(string_to_sid(":label_stack"));
    int label = svt_get_last_value(symb_value);
    printf("GOTO(E%d)\nF%d:\n", label, label);
}

void cr_if_exp()
{
    if (!is_symbol_stored(string_to_sid(":label_stack")))
        set_symbol_value(string_to_sid(":label_stack"), svt_create(label_count));

    symb_value_type *symb_value = get_symbol_value(string_to_sid(":label_stack"));
    int label = svt_get_last_value(symb_value);
    svt_remove_last_value(symb_value);

    printf("E%d:\n", label);

    symb_value = get_symbol_value(string_to_sid(":if_offset_save"));
    offset = svt_get_last_value(symb_value);
    offset++;
    svt_remove_last_value(symb_value);
}

void cr_save_offset()
{
    if (!is_symbol_stored(string_to_sid(":if_offset_save")))
        set_symbol_value(string_to_sid(":if_offset_save"), svt_create(offset));

    svt_add_value(get_symbol_value(string_to_sid(":if_offset_save")), offset);
}

void cr_def_fun(char *fun_name)
{
    printf("return;\n}\n");
    stdout_to_file();
    offset = offset_save;

    for(int i = 0; i < fun_params_size; i++) {
        remove_last_value_or_symbol_if_needed(string_to_sid(fun_params[i]));
        free(fun_params[i]);
    } 
    free(fun_params);
    fun_params = NULL;
    fun_params_size = 0;
}

void cr_fun_head(char *fun_name)
{
    stdout_to_f_file();
    printf("void call_%s() {\n", fun_name);
    offset_save = offset;
    offset = 1;
}

void cr_fun_def_param(char *sid)
{
    offset++;
    cr_local_def_id(sid);
    fun_params_size++;
    fun_params = realloc(fun_params, fun_params_size*sizeof(char*));
    fun_params[fun_params_size-1] = malloc((strlen(sid)+1) * sizeof(char));
    strcpy(fun_params[fun_params_size-1], sid);
}

void cr_call_fun(char *fun_name, int nb_params)
{
    printf("CALL(call_%s)\n", fun_name);
    printf("RESTORE(%d)\n", nb_params);
}

/*
void cr_def_fun(char *concat)
{
    char *fun_name = concat;
    int param_nbr = 0;
    for (size_t i = 0; i < strlen(concat); i++)
    {
        if (concat[i] == ' ')
        {
            concat[i] = '\0';
            param_nbr++;
        }
    }

    symb_value_type *sv = get_symbol_value(string_to_sid(strcat("::", fun_name)));
    printf("GOTO(\"strcat(CALL_%s_\", (char *)stack[sp-%d]))", fun_name, param_nbr);
    printf("AFTER_FUNC_%s_%d:\n", fun_name, svt_get_last_value(sv));
}

char *cr_fun_head(char *fun_name, char *params)
{
    cr_def_id(fun_name);

    char *tmp = malloc((strlen(fun_name) + 3) * sizeof(char));
    strcat(tmp, "::");
    strcat(tmp, fun_name);

    if (!is_symbol_stored(string_to_sid(":if_offset_save")))
    {
        // fprintf(stderr, "\n ON est là %ld\n", strlen(fun_name));

        set_symbol_value(string_to_sid(tmp), svt_create(0));
        // fprintf(stderr, "\n ON est là \n");
    }
    else
    {
        symb_value_type *sv = get_symbol_value(string_to_sid(tmp));
        svt_add_value(sv, svt_get_last_value(sv) + 1);
    }

    if (!is_symbol_stored(string_to_sid(":p:f")))
    {
        set_symbol_value(string_to_sid(tmp), svt_create(0));
    }
    else
    {
        symb_value_type *sv = get_symbol_value(string_to_sid(tmp));
        svt_add_value(sv, svt_get_last_value(sv) + 1);
    }

    symb_value_type *sv = get_symbol_value(string_to_sid(tmp));

    printf("GOTO(AFTER_FUNC_%s_%d)\n", fun_name, svt_get_last_value(sv));
    printf("FUNC_%s_%d:\n", fun_name, svt_get_last_value(sv));

    char *tmp2 = malloc((strlen(fun_name) + strlen(params) + 2) * sizeof(char));
    strcat(tmp2, fun_name);
    strcat(tmp2, " ");
    strcat(tmp2, params);
    return tmp2;
}

char *cr_concat_params(char *list, char *param)
{
    fprintf(stderr, "FLSFSKL\n");
    strcat(list, " ");
    strcat(list, param);

    return list;
}
*/