/*
 *  Attribut.c
 *
 */

#include "Attribut.h"

#include <stdlib.h>

symb_value_type* svt_create(int first_value) {
    symb_value_type *sv = malloc(sizeof(symb_value_type));
    sv->values = malloc(sizeof(int));
    sv->values[0] = first_value;
    sv->values_count = 1;
    return sv;
}

void svt_free(symb_value_type *sv) {
    if(!sv)
        return;
    free(sv->values);
    free(sv);
}

void svt_add_value(symb_value_type *sv, int value) {
    sv->values_count++;
    sv->values = realloc(sv->values, sizeof(int)*sv->values_count);
    sv->values[sv->values_count-1] = value;
}

int svt_get_last_value(symb_value_type* sv) {
    return sv->values[sv->values_count-1];
}

void svt_remove_last_value(symb_value_type* sv) {
    if(!sv->values_count)
        return;
    sv->values_count--;
    sv->values = realloc(sv->values, sizeof(int)*sv->values_count);
}

void svt_print(symb_value_type* sv) {
    for(int i = 0; i < sv->values_count; i++) {
        printf("%d - ", sv->values[i]);
    }
}