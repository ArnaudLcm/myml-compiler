#ifndef COMPLEX_RULES_H
#define COMPLEX_RULES_H

extern int offset;

extern int label_count;

void cr_def_id(char* sid);

void cr_local_def_id(char* sid);

void cr_let_exp(char* sid);

void cr_then();

void cr_else();

void cr_if_exp();

void cr_save_offset();

void cr_def_fun(char*);

void cr_fun_head(char*);

void cr_fun_def_param(char*);

void cr_call_fun(char *, int);

// char* cr_concat_params(char* list, char* param);

#endif