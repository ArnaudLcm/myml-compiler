/*
 *  Table des symboles.c
 *
 *  Created by Janin on 12/10/10.
 *  Copyright 2010 LaBRI. All rights reserved.
 *
 */
#include "Table_des_symboles.h"

#include <stdlib.h>
#include <stdio.h>

extern FILE *file_in;
extern FILE *file_out;

/* The storage structure is implemented as a linked chain */

/* linked element def */

typedef struct elem
{
	sid symbol_name;
	symb_value_type *symbol_value;
	struct elem *next;
} elem;

void symb_elem_free(elem *e)
{
	if (!e)
		return;
	if (e->next)
	{
		symb_elem_free(e->next);
	}
	svt_free(e->symbol_value);
	free(e);
}

/* linked chain initial element */
static elem *storage = NULL;

/* return 1 if symb_id is in the symbol table, 0 otherwise */
unsigned int is_symbol_stored(sid symb_id)
{
	elem *tracker = storage;

	while (tracker)
	{
		if (tracker->symbol_name == symb_id)
			return 1;
		tracker = tracker->next;
	}

	return 0;
}

/* get the symbol value of symb_id from the symbol table */
symb_value_type *get_symbol_value(sid symb_id)
{
	elem *tracker = storage;
	/* look into the linked list for the symbol value */
	while (tracker)
	{
		if (tracker->symbol_name == symb_id)
			return tracker->symbol_value;
		tracker = tracker->next;
	}

	/* if not found does cause an error */
	fprintf(stderr, "Error : symbol %s has no defined value\n", (char *)symb_id);
	exit(-1);
};

/* set the value of symbol symb_id to value */
symb_value_type *set_symbol_value(sid symb_id, symb_value_type *value)
{

	elem *tracker;

	/* (optionnal) check that sid is valid symbol name and exit error if not */
	if (!sid_valid(symb_id))
	{
		fprintf(stderr, "Error : symbol id %p is not have no valid sid\n", symb_id);
		exit(-1);
	}

	/* look for the presence of symb_id in storage */

	tracker = storage;
	while (tracker)
	{
		if (tracker->symbol_name == symb_id)
		{
			tracker->symbol_value = value;
			return tracker->symbol_value;
		}
		tracker = tracker->next;
	}

	/* otherwise insert it at head of storage with proper value */

	tracker = malloc(sizeof(elem));
	tracker->symbol_name = symb_id;
	tracker->symbol_value = value;
	tracker->next = storage;
	storage = tracker;

	return storage->symbol_value;
}

/* remove the symbol corresponding to the given sid */
void remove_symbol(sid symb_id)
{
	elem *previous = NULL;
	elem *tracker = storage;
	/* look into the linked list for the symbol value */
	while (tracker)
	{
		if (tracker->symbol_name == symb_id)
		{
			if (previous)
			{
				previous->next = tracker->next;
			}
			else
			{
				storage = tracker->next;
			}
			tracker->next = NULL;
			symb_elem_free(tracker);
			return;
		}
		previous = tracker;
		tracker = tracker->next;
	}

	/* if not found does cause an error */
	fprintf(stderr, "Error : symbol %s has no defined value\n", (char *)symb_id);
	exit(-1);
}

void symb_table_free()
{
	symb_elem_free(storage);
}

void symb_table_print()
{
	printf("//==SYMBOLS TABLE==\n");
	elem *tracker = storage;
	while (tracker)
	{
		printf("//%s : ", sid_to_string(tracker->symbol_name));
		svt_print(tracker->symbol_value);
		printf("\n");
		tracker = tracker->next;
	}
	printf("//=================\n");
}

void remove_last_value_or_symbol_if_needed(sid symb_id) {
    symb_value_type *sv = get_symbol_value(symb_id);
    if (sv->values_count > 1)
    {
        svt_remove_last_value(sv);
    }
    else
    {
        remove_symbol(symb_id);
    }
}