
%{

  // header included in y.tab.h
#include "src/Attribut.h"  
#include "src/Table_des_symboles.h"
#include "src/Complex_Rules.h"
#include <stdio.h>
#include <stdlib.h> 

FILE * file_in = NULL;
FILE * file_out = NULL;
FILE * file_f_out = NULL;

extern int yylex();
extern int yyparse();

void yyerror (char* s) {
   printf("\n%s\n",s);
 }

%}

%union {
  int val_int;
  char * val_string;
  //void * mixed;
}

%token <val_int> NUM FLOAT BOOL

%token <val_string> ID STRING 

%token PV LPAR RPAR LET IN VIR

%token IF THEN ELSE

%token <val_string> ISLT ISGT ISLEQ ISGEQ ISEQ
%left ISEQ
%left ISLT ISGT ISLEQ ISGEQ


%token AND OR NOT
%left OR
%left AND

%token PLUS MOINS MULT DIV EQ
%left PLUS MOINS
%left MULT DIV
%left CONCAT

%nonassoc UNA    /* pseudo token pour assurer une priorite locale */

%type <val_string> local_def_id
%type <val_string> fun_head
%type <val_string> def_fun
%type <val_int> arg_list

%type <val_string> comp

%start prog 
 


%%

 /* a program is a list of instruction */

prog : inst PV

| prog inst PV /* {printf("Une autre instruction\n");} */
;

/* a instruction is either a value or a definition (that indeed looks like an affectation) */

inst : let_def 
| exp { printf("DROP\n"); offset--; }
;



let_def : def_id
| def_fun
;

def_id : LET ID EQ exp { cr_def_id($2); }
;

def_fun : LET fun_head EQ exp { cr_def_fun($2); }
;

fun_head : ID { cr_fun_head($1); } LPAR id_list RPAR { $$ = $1; }
;

id_list : ID { cr_fun_def_param($1); }
| id_list VIR ID { cr_fun_def_param($3); }
;


exp : arith_exp
| let_exp
;

arith_exp : MOINS arith_exp %prec UNA
| arith_exp MOINS arith_exp { printf("SUBI\n"); offset--; }
| arith_exp PLUS arith_exp { printf("ADDI\n"); offset--; }
| arith_exp DIV arith_exp { printf("DIVI\n"); offset--; }
| arith_exp MULT arith_exp { printf("MULTI\n"); offset--; }
| arith_exp CONCAT arith_exp
| atom_exp
;

atom_exp : NUM { printf("LOADI(%d)\n", $1); offset++; }
| FLOAT
| STRING
| ID { printf("LOAD(fp + %d)\n", svt_get_last_value(get_symbol_value($1))); offset++; }
| control_exp
| funcall_exp
| LPAR exp RPAR
;

control_exp : if_exp
;


if_exp : if cond then atom_exp else atom_exp { cr_if_exp(); }
;

if : IF { cr_save_offset(); } ;
cond : LPAR bool RPAR ;
then : THEN { cr_then(); } ;
else : ELSE { cr_else(); } ;

let_exp : local_def_id IN atom_exp { cr_let_exp($1); }
| local_def_id IN let_exp { cr_let_exp($1); }
;

local_def_id : LET ID EQ exp { cr_local_def_id($2); $$ = $2; }
;

funcall_exp : ID { printf("SAVEFP\n"); } LPAR arg_list RPAR { cr_call_fun($1, $4); }
;

arg_list : arith_exp { $$ = 1; }
| arg_list VIR  arith_exp { $$ = $1 + 1; }
;

bool : BOOL { printf("LOADI(%d)\n", $1); }
| bool OR bool { printf("OR\n"); offset--; }
| bool AND bool { printf("AND\n"); offset--; }
| NOT bool %prec UNA { printf("NOT\n"); }
| exp comp exp { printf("%s\n", $2); }
| LPAR bool RPAR
;


comp :  ISLT | ISGT | ISLEQ | ISGEQ | ISEQ
;

%% 
int main (int argc, char *argv[]) {
  /* The code below is just a standard usage example.
     Of course, it can be changed at will.

     for instance, one could grab input and ouput file names 
     in command line arguements instead of having them hard coded */
  
  if(argc<2) {
    return 1;
  }

  stderr = stdout;
  
  /* opening target code file and redirecting stdout on it */
 file_out = fopen("test/output.p","w");
 file_f_out = fopen("test/output.fp","w");
 stdout = file_out; 

 /* openng source code file and redirecting stdin from it */
 file_in = fopen(argv[1],"r");
 stdin = file_in; 

 /* As a starter, on may comment the above line for usual stdin as input */

 int ret = yyparse ();
 //if(ret==0)
 // printf("DROP\n");

 symb_table_free();
 sid_table_free();

 /* any open file shall be closed */
 fclose(file_out);
 fclose(file_f_out);
 fclose(file_in);

 return ret;
} 

