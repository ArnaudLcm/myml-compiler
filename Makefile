all		:	myml

y.tab.h y.tab.c :	src/myml.y
			bison -y  -d -v  src/myml.y
lex.yy.c	:	src/myml.l y.tab.h
			flex src/myml.l 
myml		:	lex.yy.c y.tab.c src/Table_des_symboles.c src/Table_des_chaines.c src/Attribut.c src/Complex_Rules.c
			gcc -o myml lex.yy.c y.tab.c src/Table_des_symboles.c src/Table_des_chaines.c src/Attribut.c src/Complex_Rules.c
clean		:	
			rm -f 	lex.yy.c *.o y.tab.h y.tab.c myml *~ y.output test/output.p test/output.fp output
execp		:
			gcc -o output output.c PCode.c && ./output