let y = 2;
let x = 1;
let g(y, x) = let z = 3 in (x+y+z);
let z = g(2,1); 

let fact(x) = if (x<=0) then 1 else (x*fact(x-1));
let a = fact(4);

let j = x + 9;

/* BEGIN STACK : fp = 0, sp = 5 */
/* (0)  2 */
/* (1)  1 */
/* (2)  6 */
/* (3)  24 */
/* (4)  10 */
/* END STACK */
