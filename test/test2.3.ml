let a = ( let x = 4 in ( let y = 9 in ( x*y ) ) );

let a = ( let x = 4 in ( ( let x = 5 in x ) + x ) );

let a = ( let x = 4 in ( x + ( let x = 5 in x ) ) );

/* BEGIN STACK : fp = 0, sp = 1 */
/* (0)  9 */
/* END STACK */
