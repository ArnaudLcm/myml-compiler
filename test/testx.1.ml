let fact(x) = if (x<=0) then 1 else (x*fact(x-1));
let plusUn(x) = x+1;

let x = plusUn(fact(4));

/* On pense que ce genre d'exemples ne marche pas car il n'y a pas de pile d'appel dans le PCode, il ne prend en compte qu'un appel de fonction. */
/* Ou alors il faudrait faire l'évaluation des paramètres avant le premier appel à SAVEFP (comme dans pas mal de langages de programmation) */
