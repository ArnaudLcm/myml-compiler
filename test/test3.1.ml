let x = 2;

let y =
if ( !x == 2 || false ) 
then 
    3 
else 
    6;

let z =
if ( x == 2 || false ) 
then 
    3 
else 
    6;

/* BEGIN STACK : fp = 0, sp = 3 */
/* (0)  2 */
/* (1)  6 */
/* (2)  3 */
/* END STACK */
