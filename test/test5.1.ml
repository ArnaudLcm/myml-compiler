let plusUn(x) = x+1;
let y = 3;
let z = plusUn(y);

/* BEGIN STACK : fp = 0, sp = 2 */
/* (0)  3 */
/* (1)  4 */
/* END STACK */
