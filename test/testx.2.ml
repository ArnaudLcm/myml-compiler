let fact(x) = if (x<=0) then 1 else (x*fact(x-1));
let plusUn(x) = fact(x);

let x = plusUn(4);
