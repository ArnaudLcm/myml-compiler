let fact(x) = if (x<=0) then 1 else (x*fact(x-1));
let x = fact(4);

/* BEGIN STACK : fp = 0, sp = 1 */
/* (0)  24 */
/* END STACK */
