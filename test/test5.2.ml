let y = 2;
let x = 1;
let g(x,y) = let z = 3 in (x+y+z);
let z = g(1,2); 
let j = x + 9;

/* BEGIN STACK : fp = 0, sp = 4 */
/* (0)  2 */
/* (1)  1 */
/* (2)  6 */
/* (3)  10 */
/* END STACK */
