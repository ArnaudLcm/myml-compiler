# MyML Compiler


## Purpose

The purpose of this project is to create a compiler for our language entitled MyML.

Here is a non exhaustiv list of some **MYML's key points** :
- Weak typing
- Support recursion
- Different scopes
- Unary operators


## Langage syntax
- Declare a variable :
```myml
let x = 0;
```
- Controls operations :
```myml
let x = 10;

if (x > 1) then 1 else 0;
```

- Functions :
```myml
let f(x,y) = x + y;
```

- Arithmetic operations :
```myml
+ - *
```


## Usage
Firstly, you have to compile the compiler :
```
make
```

After this step done, create a new myml file and write your instructrions.
In order to compile your instruction set, please use the script file compil.sh by following this syntax :
```bash
./compile.sh <PATH-TO-YOUR-MYML-FILE>
```

## Example of program

- First example :
```myml
let fact(x) = if (x<=0) then 1 else (x*fact(x-1));
let plusUn(x) = fact(x);

let x = plusUn(4);
```
- Second example :
```myml
let a = ( let x = 4 in ( let y = 9 in ( x*y ) ) );

let a = ( let x = 4 in ( ( let x = 5 in x ) + x ) );

let a = ( let x = 4 in ( x + ( let x = 5 in x ) ) );
```

## Contribute
Feel free to contribute to this project by making merge request. They will be reviewed as soon as possible.
